.PHONY: debug release build run memcheck clean format

#debug:
#	mkdir -p $(BUILD_DIR)
#	cd $(BUILD_DIR) && CC=$(CC) CXX=$(CXX) LDFLAGS=$(LDFLAGS) cmake -GNinja -DCMAKE_VERBOSE_MAKEFILE=ON -DCMAKE_BUILD_TYPE=Debug ../
#
#rel:
#	mkdir -p $(BUILD_DIR)
#	cd $(BUILD_DIR) && CC=$(CC) CXX=$(CXX) LDFLAGS=$(LDFLAGS) cmake -GNinja -DCMAKE_BUILD_TYPE=Release ../ -DAVXHTTP_BUILD_BENCHMARK=ON
## 	-DAVXHTTP_BUILD_BENCHMARK=ON -DAVXHTTP_BUILD_TEST=ON

build:
	cmake --build $(BUILD_DIR) 

run: build
	./$(BUILD_DIR)/example

bench: build
	sudo cpupower frequency-set --governor performance
	./$(BUILD_DIR)/benchmarks
	sudo cpupower frequency-set --governor schedutil
	
test: build
	./$(BUILD_DIR)/test

memcheck: build
	valgrind ./$(BUILD_DIR)/avxhttp

clean:
	rm -rf $(BUILD_DIR)

format:
	clang-format -i *.h *.c *.cpp
	clang-tidy *.cpp

	
# 	https://llvm.org/docs/Benchmarking.html
# https://lemire.me/blog/2018/03/08/iterating-over-set-bits-quickly-simd-edition/
# __builtin_ctzll
