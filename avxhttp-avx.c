 
#include <immintrin.h>

#include "avxhttp.h"

// 
// __SSE__
// __SSE2__
// __SSE3__
// __AVX__
// __AVX2__

/* errors */

const struct avxhttp_request_error avxhttp_request_errors[] = {
#define OK 0
    {"OK", 0},

#define BAD_REQUEST -1
    {"BAD_REQUEST", 400},

#define INCOMPLETE_REQUEDT -2
    {"INCOMPLETE_REQUEDT", 0},

#define METHOD_NOT_ALLOWED -3
    {"METHOD_NOT_ALLOWED", 405},

#define URL_TOO_LONG -4
    {"URL_TOO_LONG", 414},

#define HTTP_VERSION_NOT_SUPPORTED -5
    {"HTTP_VERSION_NOT_SUPPORTED", 505}

};

__attribute__((always_inline)) const struct avxhttp_request_error *avxhttp_request_strerror(ssize_t retval) {
    return &avxhttp_request_errors[-retval];
}

/* globals */

#define M128I_REPEAT_CHAR(c)                                                                                           \
    (__m128i)(__v16qi) { c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, }

#define M256I_REPEAT_CHAR(c)                                                                                           \
    (__m256i)(__v32qi) {                                                                                               \
        c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c,                \
    }
    
#define M256I_REPEAT_CHAR_16(c15, c14, c13, c12, c11, c10, c9, c8, c7, c6, c5, c4, c3, c2, c1, c0) \
    _mm256_set_epi8( \
        c15, c14, c13, c12, c11, c10, c9, c8, c7, c6, c5, c4, c3, c2, c1, c0, \
        c15, c14, c13, c12, c11, c10, c9, c8, c7, c6, c5, c4, c3, c2, c1, c0  \
    )


__m128i http11_request = (__m128i)(__v16qi){'H', 'T', 'T', 'P', '/', '1', '.', '1', '\r', '\n'};

__m256i http11_response = (__m256i)(__v32qi){'H', 'T', 'T', 'P', '/', '1', '.', '1', ' ', 0, 0, 0, ' '};

__m256i colons = M256I_REPEAT_CHAR(':');
__m256i spaces = M256I_REPEAT_CHAR(' ');
__m256i tabs = M256I_REPEAT_CHAR('\t');
__m256i crs = M256I_REPEAT_CHAR('\r');
__m256i lfs = M256I_REPEAT_CHAR('\n');

/* functins */

__attribute__((always_inline)) int avxhttp_match_mask_128(__m128i vec1, __m128i vec2) {
    __m128i cmp_mask = _mm_cmpeq_epi8(vec1, vec2);
    return _mm_movemask_epi8(cmp_mask);
}

__attribute__((always_inline)) int avxhttp_match_mask_256(__m256i vec1, __m256i vec2) {
    __m256i cmp_mask = _mm256_cmpeq_epi8(vec1, vec2);
    return _mm256_movemask_epi8(cmp_mask);
}

__attribute__((always_inline)) __m256i avxhttp_build_full_mask(__m256i chunk, __m256i table_low, __m256i table_high) {
    // lookup low nibble
    __m256i mask_low_full = _mm256_shuffle_epi8(table_low, chunk);

    //   get high nibble
    const __m256i chunk_high  = _mm256_and_si256(_mm256_srli_epi16(chunk, 4), _mm256_set1_epi8(15));

    // lookup high nibble
    __m256i mask_high_full = _mm256_shuffle_epi8(table_high, chunk_high);

    // merge nibbles
    return _mm256_and_si256(mask_low_full, mask_high_full);
}

__attribute__((always_inline)) ssize_t avxhttp_parse_headers(
    const char *buffer, const char *buffer_end, struct avxhttp_header *headers, size_t *headers_sz) {
    
    size_t header_ptr = (size_t) headers;
    size_t header_ptr_max = ((size_t) headers) + *headers_sz * sizeof(struct avxhttp_header);
            
    // 0 if looking for name, 1 if looking for value
    unsigned int mask_index = 0;
    
    const char *chunk_ptr = buffer;
    
    while(1) {        
        __m256i chunk = _mm256_loadu_si256((const __m256i_u *) chunk_ptr);
        
        // valid chars according to rfc7230 are ASCII between 0x20 and 0x7e - BUT -
        // - chars above 0x7e aren't really used for anything else
        // - matching also 0x20 matches optional whitespace in the value
        // - no one really uses tabs as separator inside headers, come on
        // so yeah this is not entirely rfc7230 compliant, but there's no harm in that (and we can make it compliant very easily at the cost of performance anyway)
        __m256i valid_chars = _mm256_cmpgt_epi8(chunk, M256I_REPEAT_CHAR(0x20)); // very brittle
        unsigned int valid_chars_mask = _mm256_movemask_epi8(valid_chars);
        
        // name mask is a bitmask of all colons, simply
        unsigned int name_mask = avxhttp_match_mask_256(chunk, M256I_REPEAT_CHAR(':'));
        
        // value mask is the valid_chars_mask OR'ed with itself to account for single spaces inside the value
        unsigned int value_mask = valid_chars_mask | (valid_chars_mask >> 1);
        
        unsigned int string_end_pos = 0;
        
        unsigned int inverted_value_mask;
                
        if (mask_index) {
            inverted_value_mask = ~value_mask;
            goto parse_value;
        }
        
        // this is an unrolled loop, so we comment only the first half and the differences
        while (1) {
            
parse_name:;

            // terminate the cycle if there's no more ones in the inverted mask
            // and tell the next iteration we were looking for the end of a header name
            if (__builtin_expect(!name_mask, 0)) {
                mask_index = 0;
                break;
            }
            
            // first 1 in the inverted mask terminates the name (it's the ":")
            string_end_pos = _tzcnt_u32(name_mask);
            const char *name_end_ptr = chunk_ptr + string_end_pos;
                      
            // assign name pointer
            const char **name_ptr = (const char **) header_ptr;
            *name_ptr = buffer;

            // assign name length
            size_t *name_len = (size_t *) (header_ptr + sizeof(const char *));
            *name_len = name_end_ptr - buffer;
            
#ifdef AVXHTTP_STRICT
            // we can check for ":" after header for better compliance, but who cares
            if (__builtin_expect(*name_end_ptr != ':', 0)) {
                return BAD_REQUEST;
            }
#endif
                        
            // move the struct pointer forward and check for overflow
            header_ptr = ((size_t) name_len) + sizeof(size_t);
            if (__builtin_expect((header_ptr >= header_ptr_max), 0)) {
                goto end;
            }
            
            string_end_pos++;
            
            // clean the first N bits of the mask (could just shift and sum to end_ptr maybe)
            valid_chars_mask = (valid_chars_mask >> string_end_pos) << string_end_pos;
            
            // if there's not a single char then skip to the first char in the next chunk
            if (__builtin_expect(!valid_chars_mask, 0)) {     
                
skip_after_name:;

                chunk_ptr += 32;
                if (__builtin_expect(chunk_ptr >= buffer_end, 0)) {
                    goto end;
                }
        
                __m256i chunk = _mm256_loadu_si256((const __m256i_u *) chunk_ptr);
                
                __m256i valid_chars = _mm256_cmpgt_epi8(chunk, M256I_REPEAT_CHAR(0x20)); 
                valid_chars_mask = _mm256_movemask_epi8(valid_chars);
                                
                name_mask = avxhttp_match_mask_256(chunk, M256I_REPEAT_CHAR(':'));
                value_mask = valid_chars_mask | (valid_chars_mask >> 1);
                
                // keep skipping if no char is found
                if (__builtin_expect(!valid_chars_mask, 0)) {
                    goto skip_after_name;
                }                
                
                unsigned int value_start = _tzcnt_u32(valid_chars_mask);
                buffer = chunk_ptr + value_start;
                
                string_end_pos = 0;
                
                inverted_value_mask = (~value_mask >> value_start) << value_start;
                
                goto parse_value;
            }
                        
            // get the start of the first char in the value and move the buffer start there
            unsigned int value_start = _tzcnt_u32(valid_chars_mask);
            buffer = chunk_ptr + value_start;
            
            // move the value_mask accordingly
            inverted_value_mask = (~value_mask >> value_start) << value_start;
            
            // like the first part but with value
parse_value:;

            if (__builtin_expect(!inverted_value_mask, 0)) {
                mask_index = 1;
                break;
            }
            
            string_end_pos = _tzcnt_u32(inverted_value_mask);
            const char *value_end_ptr = chunk_ptr + string_end_pos;
                        
            const char **value_ptr = (const char **) header_ptr;
            *value_ptr = buffer;

            size_t *value_len = (size_t *) (header_ptr + sizeof(const char *));
            *value_len = value_end_ptr - buffer;
            
            header_ptr = ((size_t) value_len) + sizeof(size_t);
            if (__builtin_expect((header_ptr >= header_ptr_max), 0)) {
                goto end;
            }
                        
            valid_chars_mask = (valid_chars_mask >> string_end_pos) << string_end_pos;
            if (__builtin_expect(!valid_chars_mask, 0)) {
                
skip_after_value:;

                chunk_ptr += 32;
                if (__builtin_expect(chunk_ptr >= buffer_end, 0)) {
                    goto end;
                }
        
                __m256i chunk = _mm256_loadu_si256((const __m256i_u *) chunk_ptr);
                
                __m256i valid_chars = _mm256_cmpgt_epi8(chunk, M256I_REPEAT_CHAR(0x20));
                valid_chars_mask = _mm256_movemask_epi8(valid_chars);
                                
                name_mask = avxhttp_match_mask_256(chunk, M256I_REPEAT_CHAR(':'));
                value_mask = valid_chars_mask | (valid_chars_mask >> 1);
                
                if (__builtin_expect(!valid_chars_mask, 0)) {
                    goto skip_after_value;
                }
                
                unsigned int name_start = _tzcnt_u32(valid_chars_mask);
                buffer = chunk_ptr + name_start;
                
                unsigned int end_headers = *(unsigned int*) (buffer-4);
                if (__builtin_expect(end_headers == 0x0a0d0a0d, 0)) {
                    goto end;
                }
                
                string_end_pos = 0;
                
                name_mask = (name_mask >> name_start) << name_start;
                
                goto parse_name;
            }

            unsigned int name_start = _tzcnt_u32(valid_chars_mask);
            buffer = chunk_ptr + name_start;
            
            // check if before the next name we have "\r\n\r\n"
            unsigned int end_headers = *(unsigned int*) (buffer-4);
            if (__builtin_expect(end_headers == 0x0a0d0a0d, 0)) {
                goto end;
            }
            
            name_mask = (name_mask >> name_start) << name_start;

        }
        
        chunk_ptr += 32;
        if (__builtin_expect(chunk_ptr >= buffer_end, 0)) {
            goto end;
        }

    }

end:;
    *headers_sz = (header_ptr - (size_t) headers) / sizeof(struct avxhttp_header);

    return (ssize_t) buffer;
}

ssize_t avxhttp_parse_request(const char *buffer, const char *buffer_end, const char **method, size_t *method_len,
    const char **path, size_t *path_len, struct avxhttp_header *headers, size_t *headers_sz) {

    /* parse the method */

    __m256i chunk = _mm256_load_si256((const __m256i *) buffer);
    int space_mask = avxhttp_match_mask_256(chunk, spaces);

    if (!space_mask) {
        return METHOD_NOT_ALLOWED;
    }

    *method = buffer;
    *method_len = _tzcnt_u32(space_mask);

    /* parse the path */

    *path = buffer + *method_len + 1;

    // try if the path fits in the first chunk
    space_mask = _blsr_u32(space_mask);
    if (space_mask) {
        goto parse_path;
    }

    buffer += 32;
    while (1) {
        __m256i chunk = _mm256_load_si256((const __m256i *) buffer);
        space_mask = avxhttp_match_mask_256(chunk, spaces);

        if (space_mask) {
            goto parse_path;
        }

        buffer += 32;
        if (buffer >= buffer_end) {
            return URL_TOO_LONG;
        }
    }

parse_path:;

    unsigned int first_set = _tzcnt_u32(space_mask);
    const char *path_end = buffer + first_set;

    *path_len = (size_t)(path_end - *path);

    buffer += first_set + 1;

    // there must at least be ""HTTP/1.1\r\n\r\n" (length == 12)
    if (buffer_end - buffer < 12) {
        return INCOMPLETE_REQUEDT;
    }

    /* check that "HTTP/1.1\r\n" matches */

    __m128i http11_chunk = _mm_loadu_si128((const __m128i_u *) buffer);
    int is_http11_mask = avxhttp_match_mask_128(http11_chunk, http11_request);

    // 1023 -> first 10 bits are 1s, "HTTP/1.1\r\n" (length == 10)
    if ((is_http11_mask & 1023) != 1023) {
        return HTTP_VERSION_NOT_SUPPORTED;
    }

    buffer += 10;

    return avxhttp_parse_headers(buffer, buffer_end, headers, headers_sz);
}

ssize_t avxhttp_parse_response(
    const char *buffer, const char *buffer_end, int *status, struct avxhttp_header *headers, size_t *headers_sz) {

    /* check that "HTTP/1.1 XXX " matches */

    // smallest response is "HTTP/1.1 200 \r\n" (length == 15)
    // could be an incomplete response, but if the servers gives 16 bit at a time, it's 100% an error
    if (buffer_end - buffer < 15) {
        return -1;
    }

    __m256i chunk = _mm256_load_si256((const __m256i *) buffer);
    int is_http11_mask = avxhttp_match_mask_256(chunk, http11_response);

    // 8191 -> first 13 bits are 1s, "HTTP/1.1 XXX " (length == 13)
    // 4607 -> 1 000 1 1111 1111 (" XXX 1.1/HTTP")
    if ((is_http11_mask & 8191) != 4607) {
        return -1;
    }

    /* check for validity and convert status code */

    // first number must not be zero
    int status_high = buffer[9];
    if (status_high < 49 || status_high > 57) {
        return -1;
    }

    int status_mid = buffer[10];
    if (status_mid < 48 || status_mid > 57) {
        return -1;
    }

    int status_low = buffer[11];
    if (status_low < 48 || status_low > 57) {
        return -1;
    }

    *status = (status_high - 48) * 100 + (status_mid - 48) * 10 + (status_low - 48);

    /* skip over the reason phrase (RFC7230) */

    // try with the chunk we already loaded first
    int first_string_start = 0;

    // 8191 -> cut first 13 chars
    int cr_mask = avxhttp_match_mask_256(chunk, crs) & (8191 << 19);
    int lf_mask = avxhttp_match_mask_256(chunk, lfs);

    int eol_mask = (cr_mask & (lf_mask >> 1));

    if (eol_mask != 0) {
        goto parse_headers;
    }

    buffer += 32;
    if (buffer >= buffer_end) {
        return -2;
    }

    int is_last_end_cr = (cr_mask >> 31) & 1;
    while (1) {
        __m256i chunk = _mm256_load_si256((const __m256i *) buffer);

        int cr_mask = avxhttp_match_mask_256(chunk, crs);
        int lf_mask = avxhttp_match_mask_256(chunk, lfs);

        int is_first_lf = lf_mask & 1;
        first_string_start = is_last_end_cr & is_first_lf;

        eol_mask = (cr_mask & (lf_mask >> 1)) | first_string_start;

        if (__builtin_expect(eol_mask != 0, 1)) {
            goto parse_headers;
        }

        buffer += 32;
        if (buffer >= buffer_end) {
            return -2;
        }

        is_last_end_cr = (cr_mask >> 31) & 1;
    }

parse_headers:;

    unsigned int eol_distance = _tzcnt_u32(eol_mask);
    buffer += eol_distance + 2 - first_string_start;

    if (buffer >= buffer_end) {
        return -2;
    }

    return avxhttp_parse_headers(buffer, buffer_end, headers, headers_sz);
}
