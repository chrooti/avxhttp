
#include <stdio.h>
#include <string.h>

#include "avxhttp.h"

_Alignas(32) char req[] =
    "POST /cgi-bin/process.cgicgi-bin/process.cgicgi-bin/process.cgicgi-bin/process.cgicgi-bin/"
    "process.cgicgi-bin/process.cgicgi-bin/process.cgicgi-bin/process.cgicgi-bin/process.cgi HTTP/1.1\r\n"
    "User-Agent:  Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n"
    "Host:www.tutorialspoint.com\r\n"
    "Content-Type:     application/"
    "x-www-form-urlencodeform-urlencodeform-urlencodeform-urlencodeform-urlencodeform-urlencodeform-"
    "urlencodeform-urlencodeform-urlencodeform-urlencodeform-urlencoded\r\n"
    "Content-Length: length\r\n"
    "Authorization: :Bearer : eifaeoifra\r\n"
    "Accept-Language:    en-us        \r\n"
    "Accept-Encoding: gzip, deflate           \r\n"
    "Connection:    Keep-Alive  \r\n"
    "\r\n"
    "licenseID=string&content=string&/paramsXML=string";

_Alignas(32) const char res[] =
    "HTTP/1.1 200 "
    "VerylongstringVerylongstringVerylongstringVerylongstringVerylongstringVerylongstringVerylongstringVerylongstringVe"
    "rylongstringVerylongstringVerylongstringVerylongstringVerylongstringVerylongstringVerylongstring\r\n"
    "User-Agent:     Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n"
    "Host:        www.tutorialspoint.com\r\n"
    "Content-Type: "
    "application/"
    "x-www-form-urlencodeform-urlencodeform-urlencodeform-urlencodeform-urlencodeform-urlencodeform-"
    "urlencodeform-urlencodeform-urlencodeform-urlencodeform-urlencoded\r\n    "
    "Content-Length:              length    \r\n"
    "Accept-Language: en-us\r\n"
    "Accept-Encoding: gzip, deflate\r\n"
    "Connection: Keep-Alive\r\n"
    "\r\n";

void avxhttp_print_headers(struct avxhttp_header *headers, size_t headers_sz) {
    for (size_t i = 0; i < headers_sz; i++) {
        fprintf(stderr,"\"%.*s\" : \"%.*s\"\n\n", (int) headers[i].name_len, headers[i].name, (int) headers[i].value_len, headers[i].value);
    }
}

void avxhttp_print_request(ssize_t retval, const char *method, size_t method_len, const char *path, size_t path_len,
    struct avxhttp_header *headers, size_t headers_sz) {
    if (retval < 0) {
        printf("Request error!\n\n");
        return;
    }
    printf("Request received with method: %.*s and path: %.*s|\n\n", (int) method_len, method, (int) path_len, path);
    avxhttp_print_headers(headers, headers_sz);
}

void avxhttp_print_response(ssize_t retval, int status, struct avxhttp_header *headers, size_t headers_sz) {
    if (retval < 0) {
        printf("Response error!\n\n");
        return;
    }
    printf("Response received with status: %d\n\n", status);
    avxhttp_print_headers(headers, headers_sz);
}

int main() {
    ssize_t retval;
    struct avxhttp_header headers[512];
    size_t headers_sz = 10;

    const char *method;
    size_t method_len;
    const char *path;
    size_t path_len;

    retval = avxhttp_parse_request(req, req + sizeof(req), &method, &method_len, &path, &path_len, headers, &headers_sz);
    avxhttp_print_request(retval, method, method_len, path, path_len, headers, headers_sz);

    headers_sz = 512;
    int status;

    retval = avxhttp_parse_response(res, res + sizeof(res), &status, headers, &headers_sz);
    avxhttp_print_response(retval, status, headers, headers_sz);
}
