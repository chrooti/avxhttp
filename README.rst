*******
AVXHttp
*******

| Parser for HTTP/1.1 that uses AVX2.
| Benchmark ran on and AMD Ryzen 3900x.

.. code::

    ------------------------------------------------------------------------------------------------
    Benchmark                                                      Time             CPU   Iterations
    ------------------------------------------------------------------------------------------------
    BM_avxhttp_request/truncated                                3.20 ns         3.20 ns    218896886
    BM_avxhttp_request/normal/process_time                      38.1 ns         38.0 ns     18363079
    BM_avxhttp_response/truncated/process_time                  4.41 ns         4.41 ns    158149457
    BM_avxhttp_response/normal/process_time                     46.1 ns         46.1 ns     15166741
    BM_picohttpparser_request/truncated/process_time            14.4 ns         14.4 ns     48028962
    BM_picohttpparser_request/normal/process_time               87.2 ns         87.2 ns      8045610
    BM_picohttpparser_request/avx_truncated/process_time        15.4 ns         15.4 ns     45079163
    BM_picohttpparser_request/avx_normal/process_time           72.3 ns         72.2 ns      9904069
    BM_picohttpparser_response/truncated/process_time           9.27 ns         9.27 ns     75448420
    BM_picohttpparser_response/normal/process_time              96.5 ns         96.5 ns      7349046
    BM_picohttpparser_response/avx_truncated/process_time       16.1 ns         16.1 ns     43788958
    BM_picohttpparser_response/avx_normal/process_time          66.2 ns         66.2 ns     10538957
