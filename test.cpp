#include <stdlib.h>
#include <string.h>

#include "vendor/googletest/googletest/include/gtest/gtest.h"

#include "avxhttp.h"

TEST(BasicFunctionality, ParsesValidRequest) {
    alignas(32) const char req[] =
        "POST /cgi-bin/process.cgicgi-bin/process.cgicgi-bin/process.cgicgi-bin/process.cgicgi-bin/"
        "process.cgicg\ni-bin/process.cgicgi-bin/process.cgicgi-bin/process.cgicgi-bin/process.cgi HTTP/1.1\r\n"
        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n"
        "Host: www.tutorialspoint.com\r\n"
        "Content-Type: "
        "\n\rapplication/"
        "x-www-form-urlencodeform-urlencodeform-urlencodeform-urlencodeform-urlencodeform-urlencodeform-urlencodeform-"
        "urlencodeform-urlencodeform-urlencodeform-urlencoded\r\n"
        "Content-Length: length\r\n"
        "Accept-Language: en-us\r\n"
        "Accept-Encoding: gzip, deflate\r\n"
        "Connection: Keep-Alive\r\n"
        "\r\n"
        "licenseID=string&content=string&/paramsXML=string";

    struct avxhttp_header headers[512];
    size_t headers_sz = 512;

    const char *method;
    size_t method_len;

    const char *path;
    size_t path_len;

    ssize_t retval =
        avxhttp_parse_request(req, req + sizeof(req), &method, &method_len, &path, &path_len, headers, &headers_sz);

    // check that the request is valid
    EXPECT_EQ(retval > 0, 1);

    // check the method
    const char correct_method[] = "POST";
    size_t correct_method_len = sizeof(correct_method) - 1;
    EXPECT_EQ(strncmp(method, correct_method, correct_method_len), 0);
    EXPECT_EQ(method_len, correct_method_len);

    // check the path
    const char correct_path[] =
        "/cgi-bin/process.cgicgi-bin/process.cgicgi-bin/process.cgicgi-bin/process.cgicgi-bin/process.cgicg\ni-bin/"
        "process.cgicgi-bin/process.cgicgi-bin/process.cgicgi-bin/process.cgi";
    size_t correct_path_len = sizeof(correct_path) - 1;
    EXPECT_EQ(strncmp(path, correct_path, correct_path_len), 0);
    EXPECT_EQ(path_len, correct_path_len);

    // check the headers
    struct avxhttp_header correct_headers[] = {{"User-Agent", 0, "Mozilla/4.0 (compatible; MSIE5.01; Windows NT)", 0},
        {"Host", 0, "www.tutorialspoint.com", 0},
        {"Content-Type", 0,
            "\n\rapplication/"
            "x-www-form-urlencodeform-urlencodeform-urlencodeform-urlencodeform-urlencodeform-urlencodeform-"
            "urlencodeform-urlencodeform-urlencodeform-urlencodeform-urlencoded",
            0},
        {"Content-Length", 0, "length", 0}, {"Accept-Language", 0, "en-us", 0},
        {"Accept-Encoding", 0, "gzip, deflate", 0}, {"Connection", 0, "Keep-Alive", 0}};
    size_t correct_headers_size = sizeof(correct_headers) / sizeof(correct_headers[0]);
    for (size_t i = 0; i < correct_headers_size; i++) {
        struct avxhttp_header header = headers[i];
        struct avxhttp_header correct_header = correct_headers[i];

        EXPECT_EQ(strncmp(header.name, correct_header.name, header.name_len), 0);
        EXPECT_EQ(header.name_len, strlen(correct_header.name));
        EXPECT_EQ(strncmp(header.value, correct_header.value, header.value_len), 0);
        EXPECT_EQ(header.value_len, strlen(correct_header.value));
    }

    EXPECT_EQ(strcmp((const char *) retval, "licenseID=string&content=string&/paramsXML=string"), 0);
}

TEST(BasicFunctionality, ParsesValidResponse) {
    alignas(32) const char res[] = "HTTP/1.1 200 Requested range not satisfiabdfadfa\r\n"
                                   "Date: Mon, 27 Jul 2009 12:28:53 GMT\r\n"
                                   "Server: Apache/2.2.14 (Win32)\r\n"
                                   "Last-Modified: Wed, 22 Jul 2009 19:15:56 GMT\r\n"
                                   "Content-Length: 88\r\n"
                                   "Content-Type: text/html\r\n"
                                   "Connection: Closed\r\n"
                                   "\r\n";

    struct avxhttp_header headers[512];
    size_t headers_sz = 512;

    int status;

    ssize_t retval = avxhttp_parse_response(res, res + sizeof(res), &status, headers, &headers_sz);

    // check that the request is valid
    EXPECT_EQ(retval > 0, 1);

    // check the path
    EXPECT_EQ(status, 200);

    // check the headers
    struct avxhttp_header correct_headers[] = {{"Date", 0, "Mon, 27 Jul 2009 12:28:53 GMT", 0},
        {"Server", 0, "Apache/2.2.14 (Win32)", 0}, {"Last-Modified", 0, "Wed, 22 Jul 2009 19:15:56 GMT", 0},
        {"Content-Length", 0, "88", 0}, {"Content-Type", 0, "text/html", 0}, {"Connection", 0, "Closed", 0}};
    size_t correct_headers_size = sizeof(correct_headers) / sizeof(correct_headers[0]);
    for (size_t i = 0; i < correct_headers_size; i++) {
        struct avxhttp_header header = headers[i];
        struct avxhttp_header correct_header = correct_headers[i];

        EXPECT_EQ(strncmp(header.name, correct_header.name, header.name_len), 0);
        EXPECT_EQ(header.name_len, strlen(correct_header.name));
        EXPECT_EQ(strncmp(header.value, correct_header.value, header.value_len), 0);
        EXPECT_EQ(header.value_len, strlen(correct_header.value));
    }

    EXPECT_EQ(retval - sizeof(res) + 1, (size_t) res);
}

TEST(FailGracefully, AvoidOverflowWhenLoadingLastChunkInRequest) {
    alignas(32) char *req = (char *) malloc(10 * sizeof(char));
    strncpy(req, "POST /cgi-bin/process.cgicgi-bin/p", 10);

    struct avxhttp_header headers[512];
    size_t headers_sz = 512;

    const char *method;
    size_t method_len;

    const char *path;
    size_t path_len;

    ssize_t retval = avxhttp_parse_request(req, req + 10, &method, &method_len, &path, &path_len, headers, &headers_sz);

    EXPECT_EQ(retval, -1);
}

TEST(FailGracefully, AvoidOverflowWhenLoadingLastChunkInResponse) {}

TEST(FailGracefully, AvoidOverflowWhenLoadingLastChunkInHeaders) {}
