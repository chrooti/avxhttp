#include <stdlib.h>

#include "vendor/benchmark/include/benchmark/benchmark.h"

#include "avxhttp.h"

// clang-format off
#include "vendor/picohttpparser/picohttpparser.h" // NOLINT
#include "vendor/picohttpparser-avx2/avx-picohttpparser.h"
// clang-format on

alignas(32) const
    char truncated_req[] = "POST /cgi-bin/process.cgicgi-bin/process.cgicgi-bin/process.cgicgi-bin/process.cgicgi-bin/";
size_t truncated_req_len = sizeof(truncated_req);
const char *truncated_req_end = truncated_req + truncated_req_len;

alignas(32) const
    char req[] = "POST /cgi-bin/process.cgicgi-bin/process.cgicgi-bin/process.cgicgi-bin/process.cgicgi-bin/"
                 "process.cgicgi-bin/process.cgicgi-bin/process.cgicgi-bin/process.cgicgi-bin/process.cgi HTTP/1.1\r\n"
                 "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n"
                 "Host: www.tutorialspoint.com\r\n"
                 "Content-Type: "
                 "application/"
                 "x-www-form-urlencodeform-urlencodeform-urlorm-urlencoded\r\n"
                 "Content-Length: length\r\n"
                 "Accept-Language: en-us\r\n"
                 "Accept-Encoding: gzip, deflate\r\n"
                 "Connection: Keep-Alive\r\n"
                 "\r\n"
                 "licenseID=string&content=string&/paramsXML=string";
size_t req_len = sizeof(req);
const char *req_end = req + req_len;

alignas(32) const char truncated_res[] = "HTTP/1.1 200 Requested range not satisfiab/";
size_t truncated_res_len = sizeof(truncated_res);
const char *truncated_res_end = truncated_res + truncated_res_len;

alignas(32) const char res[] =
    "HTTP/1.1 200 "
    "VerylongstringVerylongstringVerylongstringVerylongstringVerylongstringVerylongstringVerylongstringVerylongstringVe"
    "rylongstringVerylongstringVerylongstringVerylongstringVerylongstringVerylongstringVerylongstring\r\n"
    "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n"
    "Host: www.tutorialspoint.com\r\n"
    "Content-Type: "
    "application/"
    "x-www-form-urlencodeform-urlencodeform-urlencodeform-urlencodeform-urlencodeform-urlencodeform-"
    "urlencodeform-urlencodeform-urlencodeform-urlencodeform-urlencoded\r\n"
    "Content-Length: length\r\n"
    "Accept-Language: en-us\r\n"
    "Accept-Encoding: gzip, deflate\r\n"
    "Connection: Keep-Alive\r\n"
    "\r\n";
size_t res_len = sizeof(res);
const char *res_end = res + res_len;

template <typename _request, typename _request_end>
static void BM_avxhttp_request(benchmark::State &state, _request req, _request_end req_end) {
    const char *method;
    size_t method_len;

    const char *path;
    size_t path_len;

    struct avxhttp_header headers[512];
    size_t headers_sz = 512;

    for (auto _ : state) {
        avxhttp_parse_request(req, req_end, &method, &method_len, &path, &path_len, headers, &headers_sz);
    }
}
BENCHMARK_CAPTURE(BM_avxhttp_request, truncated, truncated_req, truncated_req_end);
BENCHMARK_CAPTURE(BM_avxhttp_request, normal, req, req_end)->MeasureProcessCPUTime();

template <typename _response, typename _response_end>
static void BM_avxhttp_response(benchmark::State &state, _response res, _response_end res_end) {
    int status;

    struct avxhttp_header headers[512];
    size_t headers_sz = 512;

    for (auto _ : state) {
        avxhttp_parse_response(res, res_end, &status, headers, &headers_sz);
    }
}
BENCHMARK_CAPTURE(BM_avxhttp_response, truncated, truncated_res, truncated_res_end)->MeasureProcessCPUTime();
BENCHMARK_CAPTURE(BM_avxhttp_response, normal, res, res_end)->MeasureProcessCPUTime();

template <typename _parse_fn, typename _request, typename _request_len>
static void BM_picohttpparser_request(benchmark::State &state, _parse_fn parse_fn, _request req, _request_len req_len) {
    const char *method;
    size_t method_len;

    const char *path;
    size_t path_len;

    int minor_version;

    struct phr_header headers[512];
    size_t headers_sz = 512;

    for (auto _ : state) {
        parse_fn(req, req_len, &method, &method_len, &path, &path_len, &minor_version, headers, &headers_sz, 0);
    }
}
BENCHMARK_CAPTURE(BM_picohttpparser_request, truncated, phr_parse_request, truncated_req, truncated_req_len)
    ->MeasureProcessCPUTime();
BENCHMARK_CAPTURE(BM_picohttpparser_request, normal, phr_parse_request, req, req_len)->MeasureProcessCPUTime();
BENCHMARK_CAPTURE(BM_picohttpparser_request, avx_truncated, avx_phr_parse_request, truncated_req, truncated_req_len)
    ->MeasureProcessCPUTime();
BENCHMARK_CAPTURE(BM_picohttpparser_request, avx_normal, avx_phr_parse_request, req, req_len)->MeasureProcessCPUTime();

template <typename _parse_fn, typename _response, typename _response_len>
static void BM_picohttpparser_response(
    benchmark::State &state, _parse_fn parse_fn, _response res, _response_len res_len) {
    int minor_version;

    int status;

    const char *msg;
    size_t msg_len;

    struct phr_header headers[512];
    size_t headers_sz = 512;

    for (auto _ : state) {
        parse_fn(res, res_len, &minor_version, &status, &msg, &msg_len, headers, &headers_sz, 0);
    }
}
BENCHMARK_CAPTURE(BM_picohttpparser_response, truncated, phr_parse_response, truncated_res, truncated_res_len)
    ->MeasureProcessCPUTime();
BENCHMARK_CAPTURE(BM_picohttpparser_response, normal, phr_parse_response, res, res_len)->MeasureProcessCPUTime();
BENCHMARK_CAPTURE(BM_picohttpparser_response, avx_truncated, avx_phr_parse_response, truncated_res, truncated_res_len)
    ->MeasureProcessCPUTime();
BENCHMARK_CAPTURE(BM_picohttpparser_response, avx_normal, avx_phr_parse_response, res, res_len)
    ->MeasureProcessCPUTime();

BENCHMARK_MAIN();
