#ifndef AVXHTTP_H
#define AVXHTTP_H

#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

struct avxhttp_header {
    const char *name;
    size_t name_len;
    const char *value;
    size_t value_len;
};

struct avxhttp_request_error {
    const char *string;
    int http_status;
};

ssize_t avxhttp_parse_request(const char *buffer, const char *buffer_end, const char **method, size_t *method_len,
    const char **path, size_t *path_len, struct avxhttp_header *headers, size_t *headers_sz);
ssize_t avxhttp_parse_response(
    const char *buffer, const char *buffer_end, int *status, struct avxhttp_header *headers, size_t *headers_sz);
const struct avxhttp_request_error *avxhttp_request_strerror(ssize_t retval);

#ifdef __cplusplus
}
#endif

#endif // #ifndef AVXHTTP_H
